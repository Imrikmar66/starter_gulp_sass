const { series, watch, src, dest } = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create();

function styles(cb) {

    src('./app/scss/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe( dest('./app/css') )
    .pipe(browserSync.stream());

    cb();
}

function sync() {

    browserSync.init({
        server: "./app"
    });

}

function reload(cb) {

    browserSync.reload(); 
    cb();

}

function main() {

    sync();

    watch('./app/scss/*.scss', styles);
    watch('./app/index.html', reload);

}

exports.default = main;